import path from 'path'
import webpack from 'webpack';
var CopyWebpackPlugin = require('copy-webpack-plugin');

export default {
  devtools: 'eval-source-map',
  entry: {
    login: [
        'webpack-hot-middleware/client',
        path.join(__dirname, '/client/index.js')
        ],
      tournament: path.join(__dirname, '/client/tournament.js')
  },
  output: {
    filename: '[name].bundle.js',
    path: '/',
    publicPath: '/'
  },
  plugins: [
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),

  ],

  module: {
    loaders: [
      {
        test: /\.js$/,
        include: [
          path.join(__dirname, 'client'),
          path.join(__dirname, 'server/shared'),
          path.join(__dirname, 'public')
        ],
        loaders: [ 'react-hot', 'babel' ]
      }
    ]
  },
  resolve: {
    extentions: [ '', '.js' ]
  }
}
