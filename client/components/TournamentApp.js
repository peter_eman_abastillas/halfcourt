import React from 'react';
class TournamentApp extends React.Component {
    render() {
        return (
            <div className="col-lg-12">
                <h1 className="page-header">Create Tournament</h1>
                {this.props.children}
            </div>
        );
    }
}

export default TournamentApp;
