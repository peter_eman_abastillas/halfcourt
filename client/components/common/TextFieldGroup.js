import React from 'react';
import classnames from 'classnames';

const TextFieldGroup = ({ field, value, label, error, type, onChange, checkUserExists }) => {
    let placeholder = label + '...';
  return (

    <div className={classnames('form-group', { 'has-error': error })}>
      <label className="sr-only">{label}</label>
      <input
        onChange={onChange}
        onBlur={checkUserExists}
        value={value}
        type={type}
        name={field}
        className="form-control"
        placeholder={placeholder}
      />
    {error && <span className="help-block">{error}</span>}
    </div>  );
}
/*<div className="form-group">
    <label className="sr-only" for="form-last-name">Last name</label>
    <input type="text" name="form-last-name" placeholder="Last name..." className="form-last-name form-control" id="form-last-name">
</div>*/
TextFieldGroup.propTypes = {
  field: React.PropTypes.string.isRequired,
  value: React.PropTypes.string.isRequired,
  label: React.PropTypes.string.isRequired,
  error: React.PropTypes.string,
  type: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func.isRequired,
  checkUserExists: React.PropTypes.func
}

TextFieldGroup.defaultProps = {
  type: 'text'
}

export default TextFieldGroup;
