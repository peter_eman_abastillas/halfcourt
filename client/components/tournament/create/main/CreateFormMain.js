/**
 * Created by peteremanabastillas on 10/11/2016.
 */
import React from 'react';
export default class CreateFormMain extends React.Component {
    render() {
        return (            <form role="form" className=" col-xs-5">
                <div className="form-group">
                    <label>Name</label>
                    <input className="form-control"/>
                    <p className="help-block">Should be unique; it helps adding a year ex: Commisioners cup 2015</p>
                </div>
                <div className="form-group">
                    <label>Sports</label>
                    <input className="form-control"/>
                </div>
                <div className="input-daterange form-group" id="datepicker">
                    <label>Schedule Dates</label>
                    <div className="input-group">
                        <input type="text" className="input-sm form-control" name="start"/>
                        <span className="input-group-addon">to</span>
                        <input type="text" className="input-sm form-control" name="end"/>
                    </div>
                </div>
                <div className="form-group">
                    <label>Tournament Type</label>
                    <input className="tagsinput-tournament-type form-control"/>
                    <p className="help-block"></p>
                </div>
            </form>
        );
    }
}