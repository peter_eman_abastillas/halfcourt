import React from 'react';
import { connect } from 'react-redux';
import CreateFormMain from './CreateFormMain';
import { addFlashMessage } from '../../../../actions/flashMessages.js';

export default class CreateTourney extends React.Component {
    render() {
        // const { name, sports, type } = this.props;
        return (
            <div className="row">
                <CreateFormMain/>
            </div>

        );
    }
}

// CreateTourney.propTypes = {
//     name: React.PropTypes.func.isRequired,
//     sports: React.PropTypes.func.isRequired,
//     type: React.PropTypes.func.isRequired
// };


//export default connect(null, { userSignupRequest, addFlashMessage, isUserExists })();
