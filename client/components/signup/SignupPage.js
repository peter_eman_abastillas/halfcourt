import React from 'react';
import SignupForm from './SignupForm';
import { connect } from 'react-redux';
import { userSignupRequest, isUserExists } from '../../actions/signupActions';
import { addFlashMessage } from '../../actions/flashMessages.js';

class SignupPage extends React.Component {
  render() {
    const { userSignupRequest, addFlashMessage, isUserExists } = this.props;
    return (
    <div className="form-box">
      <div className="form-top">
        <div className="form-top-left">
          <h3>Sign up now</h3>
          <p>Fill in the form below to get instant access:</p>
        </div>
        <div className="form-top-right">
          <i className="fa fa-pencil"></i>
        </div>
      </div>
      <div className="form-bottom">
        <SignupForm
            isUserExists={isUserExists}
            userSignupRequest={userSignupRequest}
            addFlashMessage={addFlashMessage} />
      </div>
    </div>
    );
  }
}
/*

 <div className="row">
 <div className="col-md-4 col-md-offset-4">
 <SignupForm
 isUserExists={isUserExists}
 userSignupRequest={userSignupRequest}
 addFlashMessage={addFlashMessage} />
 </div>
 </div>
 */
SignupPage.propTypes = {
  userSignupRequest: React.PropTypes.func.isRequired,
  addFlashMessage: React.PropTypes.func.isRequired,
  isUserExists: React.PropTypes.func.isRequired
}


export default connect(null, { userSignupRequest, addFlashMessage, isUserExists })(SignupPage);
