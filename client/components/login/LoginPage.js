import React from 'react';
import LoginForm from './LoginForm';

class LoginPage extends React.Component {
  render() {
    return (
      <div className="form-box">
            <div className="form-top">
              <div className="form-top-left">
                    <h3>Sign up now</h3>
                    <p>Fill in the form below to get instant access:</p>
              </div>
              <div className="form-top-right">
                    <i className="fa fa-pencil"></i>
              </div>
            </div>
            <div className="col-md-4 col-md-offset-4">
                <LoginForm />
            </div>
      </div>
    );
  }
}

export default LoginPage;
