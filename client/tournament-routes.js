import React from 'react';
import { Route, IndexRoute } from 'react-router';

import TournamentApp from './components/TournamentApp';
import TournamentPage from './components/tournament/TournamentPage';
import CreateTourney from './components/tournament/create/main/CreateTourney';


export default (
    <Route path="/tournament.html" component={TournamentApp}>
        <IndexRoute component={TournamentPage} />
        <Route path="create" component={CreateTourney} />
    </Route>
)
