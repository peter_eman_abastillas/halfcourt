var cognitoidentity,
    usersData = {},
    token = "";
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);

    if (response.status === 'connected') {
        testAPI();
    } else if (response.status === 'not_authorized') {
        document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';
    } else {
        document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1295691103783701',
        cookie     : true,  // enable cookies to allow the server to access
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.5' // use graph api version 2.5
    });


};
function fb_login() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
    FB.login(function (response) {
        var id, name;
        // Check if the user logged in successfully.
        if (response.authResponse) {
            AWS.config.update({
                region: 'us-west-2'
            });
             cognitoidentity = new AWS.CognitoIdentity({region: 'us-east-1'});
            var access_token = response.authResponse.accessToken,
                params = {
                    IdentityPoolId: 'us-east-1:e51ec7bc-c966-4b64-aad9-0104ea6e4dda',
                    Logins: {
                        'graph.facebook.com': response.authResponse.accessToken
                    }
                };
            cognitoidentity.getId(params, function (err, data) {
                var params = {
                    IdentityId: data.IdentityId,
                    Logins: {
                        'graph.facebook.com': response.authResponse.accessToken
                    }
                };
                // Obtain AWS credentials
                cognitoidentity.getOpenIdToken(params, function (err, data) {
                    if (err) console.log(err, err.stack); // an error occurred
                    else {
                        token = data;
                        FB.api('/me', function(response) {
                            name = response.name;
                        });
                        FB.api(
                            "/" + response.authResponse.userID + "/?fields=picture,email,location,sports,cover,gender",
                            "GET",
                            function (response) {
                                var apigClient = apigClientFactory.newClient({
                                    accessKey: "AKIAJI42QI5CZM3IRNRA",
                                    secretKey: "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",
                                    sessionToken: token.    Token, //OPTIONAL: If you are using temporary credentials you must include the session token
                                    region: 'eu-east-1' // OPTIONAL: The region where the API is deployed, by default this parameter is set to us-east-1
                                });


                                var body = {};

                                var additionalParams = {
                                    // If there are any unmodeled query parameters or headers that must be
                                    //   sent with the request, add them here.
                                    headers: {
                                        'Content-Type': 'application/json'
                                    },
                                    queryParams: {}
                                };



                                if (response && !response.error) {
                                    usersData = {
                                        "userID": response.id, //facebook base userID,
                                        "profile": {
                                            "name": response.name, // display name
                                            "avatar": response.picture.data.url,
                                            "email": response.email,
                                            "gender": response.gender,
                                            "link": "https://facebook.com/" + response.id,
                                            "location": response.location.name
                                        },
                                        "sports": [],
                                        "games": {}
                                    }
                                    apigClient.rootPut(params, usersData, additionalParams)
                                        .then(function(result) {
                                            console.log(result);
                                        }).catch(function(err) {
                                        console.log(err);
                                    });
                                }
                            }
                        );
                    }
                });


            });
        } else {
            console.log('There was a problem logging you in.');
        }

    }, {scope: "public_profile,email,user_location"});
}
// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
        console.log('Successful login for: ' + response.name);
        document.getElementById('status').innerHTML =
            'Thanks for logging in, ' + response.name + '!';
    });
}