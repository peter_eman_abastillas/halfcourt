var path = require('path');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var config = {
    entry: [
        path.join(__dirname, '/client/index.js'),
        path.join(__dirname, '/client/tournament.js')
    ],
    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, 'dist'),
        publicPath: '/'
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            loader: 'babel',
            query:
            {
                presets:['es2015', 'react']
            }
        }]
    },

};

module.exports = config;