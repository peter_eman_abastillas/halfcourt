import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';

import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from '../webpack.config.dev';

import users from './routes/users';
import auth from './routes/auth';
import events from './routes/events';

let public_dir = __dirname + '/';
let app = express();

app.use(bodyParser.json());

app.use('/api/users', users);
app.use('/api/auth', auth);
app.use('/api/events', events);

const compiler = webpack(webpackConfig);
console.log( webpackConfig.output.publicPath);
console.log( webpackConfig.output);

app.use(webpackMiddleware(compiler, {
  hot: true,
  publicPath: webpackConfig.output.publicPath,
  noInfo: true
}));

app.use(webpackHotMiddleware(compiler));

app.get('/*', (req, res) => {
    console.log(req.params[0]);
    if(req.params[0]=='' || req.params[0].indexOf('html')!==-1) {
        let html = req.params[0] || 'login.html';
        res.sendFile(path.join(__dirname, './../public/' + html));
    } else if(req.params[0].indexOf('assets')!==-1) {
        res.sendFile(path.join(__dirname, './../public/' + req.params[0]));
    }
});

app.get('/:id', (req, res) => {
    console.log(req.params.id);

});

app.listen(3000, () => console.log('Running on localhost:3000'));
